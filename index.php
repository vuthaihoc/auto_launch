<?php
/**
 * Created by PhpStorm.
 * User: hocvt
 * Date: 4/4/18
 * Time: 00:27
 */

ini_set( "display_errors", 1);
error_reporting(E_ALL);

require __DIR__ . "/vendor/autoload.php";
require __DIR__ . "/functions.php";

$users = include __DIR__ . "/users.php";

use Carbon\Carbon;

$days = [
	2,3,4,5,6
];

date_default_timezone_set( 'Asia/Ho_Chi_Minh');

$today = intval( date( 'w') + 1);
if($today == 1 || $today == 7){// không chạy vào thứ 7 và chủ nhật
	die();
}
$next_day = Carbon::now()->addDays( 1)->format( "Y-m-d");
$tomorrow = $today + 1;
if($today == 6){// nếu là thứ 6
	$tomorrow = 2;// thì ngày tới đặt cơm là thứ 2
	$next_day = Carbon::now()->addDays( 3)->format( "Y-m-d");
}

$notifier = \Joli\JoliNotif\NotifierFactory::create();

foreach ($users as $user => $password) {
	$client = new \GuzzleHttp\Client([
		'cookies' => new \GuzzleHttp\Cookie\CookieJar(),
		'http_errors' => false,
		'headers' => [
			'User-Agent' => 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36',
			
		]
	]);
	login($user, $password);
    check_today();
    if(in_array( $tomorrow, $days)){
		reg_tomorrow();
	}
}